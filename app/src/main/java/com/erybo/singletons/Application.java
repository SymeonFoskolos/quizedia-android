package com.erybo.singletons;

import android.content.Context;
import android.telephony.TelephonyManager;
import android.util.Log;

import com.erybo.DTOs.AuthenticationReturnDTO;
import com.erybo.Model.AuthenticationErrorObject;

import java.util.UUID;

/**
 * Created by Erybo on 2/5/16.
 */
public class Application {

    private static Application ourInstance = new Application();

    public static Application getInstance() {
        return ourInstance;
    }

    private String uid;
    private AuthenticationReturnDTO authenticationReturnDTO;

    private Application() {
    }

    public String getUid(Context context) {
        if (this.uid != null) {
            return uid;
        }else {
            final TelephonyManager tm = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);

            final String tmDevice, tmSerial, androidId;
            tmDevice = "" + tm.getDeviceId();
            tmSerial = "" + tm.getSimSerialNumber();
            androidId = "" + android.provider.Settings.Secure.getString(context.getContentResolver(), android.provider.Settings.Secure.ANDROID_ID);

            UUID deviceUuid = new UUID(androidId.hashCode(), ((long)tmDevice.hashCode() << 32) | tmSerial.hashCode());
            String deviceId = deviceUuid.toString();
            setUid(deviceId.toUpperCase());
            return deviceId.toUpperCase();
        }
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public AuthenticationReturnDTO getAuthenticationReturnDTO() {
        return authenticationReturnDTO;
    }

    public void setAuthenticationReturnDTO(AuthenticationReturnDTO authenticationReturnDTO) {
        this.authenticationReturnDTO = authenticationReturnDTO;
    }
}
