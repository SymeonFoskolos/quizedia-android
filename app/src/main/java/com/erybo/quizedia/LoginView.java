package com.erybo.quizedia;

import android.content.res.Resources;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;

import com.erybo.DTOs.AuthReqDTO;
import com.erybo.DTOs.AuthenticationReturnDTO;
import com.erybo.DTOs.DeviceMetadataDTO;
import com.erybo.DTOs.UserProfileReturnDTO;
import com.erybo.Interfaces.NetworkCallback;
import com.erybo.Model.AuthenticationErrorObject;
import com.erybo.NetworkCalls.QuizediaAuthService;
import com.erybo.NetworkCalls.QuizediaUserService;
import com.erybo.quizediasdk.NetworkConnection;
import com.erybo.NetworkCalls.QuizediaLoginService;
import com.erybo.singletons.Application;
import com.erybo.utils.CryptographyUtils;

import java.io.InputStream;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

import de.tavendo.autobahn.WebSocketException;

public class LoginView extends ParentActivity implements LoginViewFragment.ButtonClickedListener, NetworkCallback {

    final String TAG = this.getClass().getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login_view);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        if(isOnline()){
            NetworkConnection.getInstance().connectToServer(this);
        }else {
            presentMessageToUserWithToast(getStringFromResourceId(R.string.network_unavailable));
        }


        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_login_view, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void buttonClicked() {
        Log.d(TAG, "In the parent");
        new QuizediaUserService().getUserProfileCall(this, Application.getInstance().getAuthenticationReturnDTO().getUserId());
    }

    @Override
    public void networkOnOpen() {

        Log.d(TAG, "networkOnOpen");
        if(isOnline()){
            Resources res = getResources();
            InputStream in_s = res.openRawResource(R.raw.quizedia);
            new QuizediaAuthService(in_s).authenticateWithServer(this);
        }else {
            presentMessageToUserWithToast(getStringFromResourceId(R.string.network_unavailable));
        }
    }

    @Override
    public void networkOnTextMessage(String payload) {
        Log.d("In the parent", "networkOnTextMessage");
    }

    @Override
    public void networkOnClose(int code, String reason) {
        Log.d("In the parent", "networkOnClose");
    }

    @Override
    public void networkWampException(WebSocketException e) {
        Log.d("In the parent", "networkWampException");
    }

    @Override
    public void objectReturned(Object object){

        if(object instanceof AuthReqDTO){
            AuthReqDTO ref = (AuthReqDTO)object;
            NetworkConnection.getInstance().setIv(ref.getSeed());
            try {
                NetworkConnection.getInstance().setDecryptedKey(CryptographyUtils.decryptAES(ref.getEncryptedKey()));
                Log.d(TAG, NetworkConnection.getInstance().getDecryptedKey());

                new QuizediaLoginService().usernamePasswordAuthentication(this, new DeviceMetadataDTO(Build.TYPE,
                        Build.VERSION.BASE_OS,
                        Build.MODEL, Build.DEVICE,
                        "1.5", getResources().getConfiguration().locale.getLanguage()+"-"+getResources().getConfiguration().locale.getCountry(),
                        Application.getInstance().getUid(this),
                        "undefined"));
            } catch (NoSuchPaddingException e) {
                e.printStackTrace();
            } catch (InvalidKeyException e) {
                e.printStackTrace();
            } catch (NoSuchAlgorithmException e) {
                e.printStackTrace();
            } catch (IllegalBlockSizeException e) {
                e.printStackTrace();
            } catch (BadPaddingException e) {
                e.printStackTrace();
            } catch (NoSuchProviderException e) {
                e.printStackTrace();
            } catch (InvalidAlgorithmParameterException e) {
                e.printStackTrace();
            }
        }else if (object instanceof AuthenticationReturnDTO) {
            AuthenticationReturnDTO ref = (AuthenticationReturnDTO)object;

            NetworkConnection.getInstance().setIv(ref.getIv());

            try {
                ref.populateEmailPassUserIdFromDecodedJson(CryptographyUtils.decryptAESServerKey(ref.getC()));
                Application.getInstance().setAuthenticationReturnDTO(ref);
                presentMessageToUserWithToast("Authentication success");
            } catch (NoSuchPaddingException e) {
                e.printStackTrace();
            } catch (InvalidKeyException e) {
                e.printStackTrace();
            } catch (NoSuchAlgorithmException e) {
                e.printStackTrace();
            } catch (IllegalBlockSizeException e) {
                e.printStackTrace();
            } catch (BadPaddingException e) {
                e.printStackTrace();
            } catch (NoSuchProviderException e) {
                e.printStackTrace();
            } catch (InvalidAlgorithmParameterException e) {
                e.printStackTrace();
            }
        }else if(object instanceof AuthenticationErrorObject){

            AuthenticationErrorObject ref = (AuthenticationErrorObject)object;
            presentMessageToUserWithToast(getAuthenticatioErrorMessageForCode(ref.getErrorCode()));
        }else if (object instanceof UserProfileReturnDTO) {
            UserProfileReturnDTO ref = (UserProfileReturnDTO)object;
            Log.d(TAG, ref.getP().getNickname());
        }
    }
}
