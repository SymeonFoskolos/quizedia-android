package com.erybo.quizedia;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Gravity;
import android.widget.Toast;

public class ParentActivity extends AppCompatActivity {

    private enum AuthenticationErrorCodes {
        NOT_USED(0),
        USER_NOT_ACTIVE(1),
        GENERAL_ERROR(2),
        USER_ALREADY_EXISTS(3),
        USER_DOES_NOT_EXIST(4);

        private int value;

        private AuthenticationErrorCodes(int i) {
            this.value = i;
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_parent);
    }

    public boolean isOnline() {
        ConnectivityManager connMgr = (ConnectivityManager)
                getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
        return (networkInfo != null && networkInfo.isConnected());
    }

    public void presentMessageToUserWithToast(String message){
        Context context = getApplicationContext();
        int duration = Toast.LENGTH_SHORT;
        Toast toast = Toast.makeText(context, message, duration);
        toast.setGravity(Gravity.CENTER_HORIZONTAL | Gravity.CENTER_VERTICAL, 0, 0);
        toast.show();
    }

    public String getStringFromResourceId(int resourceId) {
        return getResources().getString(resourceId);
    }

    public String getAuthenticatioErrorMessageForCode (int errorCode) {

        AuthenticationErrorCodes authErrorCode = AuthenticationErrorCodes.values()[errorCode];

        switch (authErrorCode){
            case USER_NOT_ACTIVE:
                return getStringFromResourceId(R.string.user_no_longer_active);
            case GENERAL_ERROR:
                return getStringFromResourceId(R.string.general_server_error);
            case USER_ALREADY_EXISTS:
                return getStringFromResourceId(R.string.user_already_exists);
            case USER_DOES_NOT_EXIST:
                return getStringFromResourceId(R.string.user_does_not_exist);
            default:
                return getStringFromResourceId(R.string.general_server_error);

        }
    }
}
