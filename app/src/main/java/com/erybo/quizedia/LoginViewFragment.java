package com.erybo.quizedia;

import android.app.Activity;
import android.content.Context;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

/**
 * A placeholder fragment containing a simple view.
 */
public class LoginViewFragment extends Fragment {

    ButtonClickedListener buttonClickedListener;

    public LoginViewFragment() {
    }

    public interface ButtonClickedListener {
        void buttonClicked();
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_login_view, container, false);

        Button button = (Button) view.findViewById(R.id.connectToServer);
        button.setOnClickListener(new Button.OnClickListener() {
            public void onClick(View v) {
                buttonClickedListener.buttonClicked();
            }
        });
        return view;
    }
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        Activity activity;

        if (context instanceof Activity){
            activity=(Activity) context;
            try {
                buttonClickedListener = (ButtonClickedListener) activity;
            } catch (ClassCastException e) {
                throw new ClassCastException(activity.toString() + " must implement ButtonClickedListener");
            }
        }

    }
}
