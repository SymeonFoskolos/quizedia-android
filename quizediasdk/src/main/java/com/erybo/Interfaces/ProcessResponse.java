package com.erybo.Interfaces;

/**
 * Created by Erybo on 2/5/16.
 */
public interface ProcessResponse <T> {

    void process(T response);

}
