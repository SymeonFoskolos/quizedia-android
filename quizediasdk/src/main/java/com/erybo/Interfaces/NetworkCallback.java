package com.erybo.Interfaces;

import de.tavendo.autobahn.WebSocketException;

/**
 * Created by Erybo on 2/2/16.
 */
public interface NetworkCallback {

    void networkOnOpen();

    void networkOnTextMessage(String payload);

    void networkOnClose(int code, String reason);

    void networkWampException(WebSocketException e);

    void objectReturned(Object object);
}
