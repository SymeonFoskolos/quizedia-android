package com.erybo.quizediasdk;

import android.util.Log;

import com.erybo.DTOs.AuthReqDTO;
import com.erybo.Interfaces.NetworkCallback;
import com.erybo.utils.BackgroundParser;
import com.erybo.utils.CryptographyUtils;

import java.util.HashMap;
import java.util.Map;

import de.tavendo.autobahn.Autobahn;
import de.tavendo.autobahn.AutobahnConnection;

/**
 * Created by Erybo on 2/5/16.
 */
public class NetworkConnection {
    private static NetworkConnection ourInstance = new NetworkConnection();

    private String wsuri = "ws://46.103.120.119:443";
    private final String TAG = this.getClass().getSimpleName();
    private AutobahnConnection autobahnConnection;
    private String decryptedKey;
    private String iv;
    private byte[] myTempKey;
    private String base64StringEncodedCipherText;
    private byte[] decryptedKeyRaw;

    public static NetworkConnection getInstance() {

        return ourInstance;

    }

    private NetworkConnection() {
        this.setAutobahnConnection(new AutobahnConnection());
    }

    public void connectToServer(final NetworkCallback objectToCallback) {


        getAutobahnConnection().connect(wsuri, new Autobahn.SessionHandler() {

            @Override
            public void onOpen() {
                Log.d(TAG, "Status: Connected to " + wsuri);
                if (objectToCallback != null) {
                    objectToCallback.networkOnOpen();
                }
            }

            @Override
            public void onClose(int code, String reason) {
                if (objectToCallback != null) {
                    objectToCallback.networkOnClose(code, reason);
                }
            }
        });

    }

    public AutobahnConnection getAutobahnConnection() {
        return autobahnConnection;
    }

    public void setAutobahnConnection(AutobahnConnection autobahnConnection) {
        this.autobahnConnection = autobahnConnection;
    }

    public String getDecryptedKey() {
        return decryptedKey;
    }

    public void setDecryptedKey(String decryptedKey) {
        this.decryptedKey = decryptedKey;
    }

    public String getIv() {
        return iv;
    }

    public void setIv(String iv) {
        this.iv = iv;
    }

    public String getBase64StringEncodedCipherText() {
        return base64StringEncodedCipherText;
    }

    public void setBase64StringEncodedCipherText(String base64StringEncodedCipherText) {
        this.base64StringEncodedCipherText = base64StringEncodedCipherText;
    }

    public byte[] getMyTempKey() {
        return myTempKey;
    }

    public void setMyTempKey(byte[] myTempKey) {
        this.myTempKey = myTempKey;
    }

    public byte[] getDecryptedKeyRaw() {
        return decryptedKeyRaw;
    }

    public void setDecryptedKeyRaw(byte[] decryptedKeyRaw) {
        this.decryptedKeyRaw = decryptedKeyRaw;
    }
}
