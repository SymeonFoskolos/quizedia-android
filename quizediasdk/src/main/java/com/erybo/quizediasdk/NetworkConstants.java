package com.erybo.quizediasdk;

/**
 * Created by Erybo on 2/9/16.
 */
public class NetworkConstants {

    public static final String AUTHENTICATION_REQUEST = "authreq";
    public static final String AUTHENTICATION_WITH_PASSWORD = "auth";
    public static final String MATCH_REQUEST = "5";
    public static final String CANCEL_MATCH_CALL = "7";
    public static final String GET_TOPICS_DIFF = "10";
    public static final String GET_TOPICS_DETAIL_CALL = "11";
    public static final String GET_USER_INFO_CALL = "12";
    public static final String GET_LEADERBOARD_CALL = "13";
    public static final String GET_USER_PROFILE_CALL = "14";
    public static final String LOGOUT_CALL = "16";
    public static final String GET_CHAT_MESSAGE_HISTORY_FOR_USER = "19";
    public static final String FOLLOW_USER_CALL = "20";
    public static final String BLOCK_USER_CALL = "21";
    public static final String GET_LIST_OF_FOLLOWERS_FOLLOWINGS = "22";
    public static final String GET_BADGES_CALL = "23";
    public static final String CHALLENGE_RECEIVED_CALL = "24";
    public static final String CHALLENGE_REJECTED_RECEIVED = "25";
    public static final String CHALLENGE_REJECTED_OPPONENT_OFFLINE = "26";
    public static final String CHALLENGE_CANCELLED_FROM_INITIATOR = "27";
    public static final String SET_FAVORITES_CALL = "32";
    public static final String GET_FAVORITES_CALL = "35";
    public static final String GET_CHAT_NOTIFICATIONS_CALL = "36";
    public static final String GET_GENERIC_NOTIFICATIONS_CALL = "37";
    public static final String REGISTER_DEVICE_CALL = "41";
    public static final String CHANGE_SETTINGS_CALL = "42";
    public static final String SEARCH_USER_CALL = "44";
    public static final String GET_USER_SETTINGS_CALL = "45";
    public static final String GET_STORE_PRODUCTS_CALL = "46";
    public static final String BUY_PRODUCTS_CALL = "47";
    public static final String SHARE_PRODUCT_CALL = "48";
    public static final String GET_AVATARS_CALL = "49";
    public static final String REPORT_QUESTION_CALL = "50";
    public static final String ACKNOWLEDGEMENT_CALL = "51";
    public static final String BADGES_WON_PUSH_NOTIFICATION = "52";
    public static final String EXECUTE_URL_SCHEME_PUSH_NOTIFICATION = "53";
    public static final String LINK_FACEBOOK_ACCOUNT = "54";
    public static final String UNLINK_FACEBOOK_ACCOUNT = "55";
}
