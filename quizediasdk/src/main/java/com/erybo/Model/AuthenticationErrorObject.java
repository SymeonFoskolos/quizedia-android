package com.erybo.Model;

/**
 * Created by Erybo on 2/8/16.
 */
public class AuthenticationErrorObject {

    private int errorCode;
    private String message;

    public AuthenticationErrorObject(Integer errorCode) {
        this.setErrorCode(errorCode);
    }

    public int getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(int errorCode) {
        this.errorCode = errorCode;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
