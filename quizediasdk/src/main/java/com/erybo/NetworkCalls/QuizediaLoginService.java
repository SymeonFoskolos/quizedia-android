package com.erybo.NetworkCalls;

import android.util.Base64;
import android.util.Log;

import com.erybo.DTOs.AuthenticationCallDTO;
import com.erybo.DTOs.AuthenticationReturnDTO;
import com.erybo.DTOs.DeviceMetadataDTO;
import com.erybo.Interfaces.NetworkCallback;
import com.erybo.Interfaces.ProcessResponse;
import com.erybo.Model.AuthenticationErrorObject;
import com.erybo.quizediasdk.NetworkConnection;
import com.erybo.quizediasdk.NetworkConstants;
import com.erybo.utils.BackgroundParser;
import com.erybo.utils.CryptographyUtils;

import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.ObjectWriter;

import java.io.IOException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.util.HashMap;
import java.util.Map;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

import de.tavendo.autobahn.Autobahn;

/**
 * Created by Erybo on 2/5/16.
 */
public class QuizediaLoginService implements ProcessResponse<AuthenticationReturnDTO> {


    private final String TAG = this.getClass().getSimpleName();
    private NetworkCallback objectToCallback;
    private String email = "2@quizedia.com";
    private String password = "7c4a8d09ca3762af61e59520943dc26494f8941b";

    public void usernamePasswordAuthentication(final NetworkCallback objectToCallback, DeviceMetadataDTO deviceMetadataDTO) throws NoSuchPaddingException, InvalidKeyException, NoSuchAlgorithmException, IllegalBlockSizeException, BadPaddingException, NoSuchProviderException, InvalidAlgorithmParameterException {
        ObjectWriter ow = new ObjectMapper().writer().withDefaultPrettyPrinter();
        setObjectToCallback(objectToCallback);
        try {
            String authenticationCallDtoJsonString = ow.writeValueAsString(new AuthenticationCallDTO("", "", email, password, deviceMetadataDTO));

            Log.d("authentication string", authenticationCallDtoJsonString);
            Map<String, Object> json = new HashMap<String, Object>();
            json.put("c", CryptographyUtils.encryptAndBase64EncodeAES(authenticationCallDtoJsonString));
            json.put("iv", Base64.encodeToString(NetworkConnection.getInstance().getMyTempKey(), Base64.DEFAULT));

            NetworkConnection.getInstance().getAutobahnConnection().call(NetworkConstants.AUTHENTICATION_WITH_PASSWORD, Object.class, new Autobahn.CallHandler() {

                @Override
                public void onResult(Object result) {

                    if(result instanceof Integer){
                        getObjectToCallback().objectReturned(new AuthenticationErrorObject((Integer)result));
                    }else {
                        new BackgroundParser<AuthenticationReturnDTO>(AuthenticationReturnDTO.class, getInstance()).execute(result);
                    }
                }

                @Override
                public void onError(String error, String info) {
                    Log.d(error, info);
                }
            }, json);

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public QuizediaLoginService getInstance() {
        return this;
    }

    @Override
    public void process(AuthenticationReturnDTO response) {
        getObjectToCallback().objectReturned(response);
    }

    public NetworkCallback getObjectToCallback() {
        return objectToCallback;
    }

    public void setObjectToCallback(NetworkCallback objectToCallback) {
        this.objectToCallback = objectToCallback;
    }
}
