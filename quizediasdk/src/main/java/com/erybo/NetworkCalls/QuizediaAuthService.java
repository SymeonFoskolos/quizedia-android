package com.erybo.NetworkCalls;

import android.util.Log;

import com.erybo.DTOs.AuthReqDTO;
import com.erybo.Interfaces.NetworkCallback;
import com.erybo.Interfaces.ProcessResponse;
import com.erybo.quizediasdk.NetworkConnection;
import com.erybo.quizediasdk.NetworkConstants;
import com.erybo.utils.BackgroundParser;
import com.erybo.utils.CryptographyUtils;

import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;

import de.tavendo.autobahn.Autobahn;


/**
 * Created by Erybo on 2/5/16.
 */
public class QuizediaAuthService implements ProcessResponse<AuthReqDTO> {

    private final String TAG = this.getClass().getSimpleName();
    private InputStream inputStream;
    private NetworkCallback objectToCallback;

    public QuizediaAuthService(InputStream inputStream)

    {
        this.setInputStream(inputStream);
    }

    public void authenticateWithServer(final NetworkCallback objectToCallback){

        setObjectToCallback(objectToCallback);

        try {
            String tempKey = CryptographyUtils.getCipherTextRSA(getInputStream());

            Map<String, Object> json = new HashMap<String, Object>();
            json.put("c", tempKey);
            json.put("aa", 1);

            NetworkConnection.getInstance().getAutobahnConnection().call(NetworkConstants.AUTHENTICATION_REQUEST, Object.class, new Autobahn.CallHandler() {

                @Override
                public void onResult(Object result) {
                    new BackgroundParser<AuthReqDTO>(AuthReqDTO.class, getInstance()).execute(result);
                }

                @Override
                public void onError(String error, String info) {
                    Log.d(error, info);
                }
            }, json);

        }catch (Exception e){

        }
    }

    public QuizediaAuthService getInstance() {
        return this;
    }

    @Override
    public void process(AuthReqDTO response) {
        objectToCallback.objectReturned(response);
    }

    public InputStream getInputStream() {
        return inputStream;
    }

    public void setInputStream(InputStream inputStream) {
        this.inputStream = inputStream;
    }

    public NetworkCallback getObjectToCallback() {
        return objectToCallback;
    }

    public void setObjectToCallback(NetworkCallback objectToCallback) {
        this.objectToCallback = objectToCallback;
    }
}
