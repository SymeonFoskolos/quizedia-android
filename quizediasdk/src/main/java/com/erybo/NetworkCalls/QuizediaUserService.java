package com.erybo.NetworkCalls;

import android.util.Log;

import com.erybo.DTOs.UserProfileReturnDTO;
import com.erybo.Interfaces.NetworkCallback;
import com.erybo.Interfaces.ProcessResponse;
import com.erybo.Model.AuthenticationErrorObject;
import com.erybo.quizediasdk.NetworkConnection;
import com.erybo.quizediasdk.NetworkConstants;
import com.erybo.utils.BackgroundParser;

import de.tavendo.autobahn.Autobahn;

/**
 * Created by Erybo on 2/9/16.
 */
public class QuizediaUserService implements ProcessResponse<UserProfileReturnDTO> {

    private final String TAG = this.getClass().getSimpleName();
    private NetworkCallback objectToCallback;

    public void getUserProfileCall (NetworkCallback objectToCallback, String userId){
        setObjectToCallback(objectToCallback);

        NetworkConnection.getInstance().getAutobahnConnection().call(NetworkConstants.GET_USER_PROFILE_CALL, Object.class, new Autobahn.CallHandler() {

            @Override
            public void onResult(Object result) {

                Log.d(TAG, result.toString());
                if (result instanceof Integer) {
                    getObjectToCallback().objectReturned(new AuthenticationErrorObject((Integer) result));
                } else {
                    new BackgroundParser<UserProfileReturnDTO>(UserProfileReturnDTO.class, getInstance()).execute(result);
                }
            }

            @Override
            public void onError(String error, String info) {
                Log.d(error, info);
            }
        }, userId);
    }

    @Override
    public void process(UserProfileReturnDTO response) {
        getObjectToCallback().objectReturned(response);
    }

    public QuizediaUserService getInstance() {
        return this;
    }

    public String getTAG() {
        return TAG;
    }

    public NetworkCallback getObjectToCallback() {
        return objectToCallback;
    }

    public void setObjectToCallback(NetworkCallback objectToCallback) {
        this.objectToCallback = objectToCallback;
    }
}
