package com.erybo.utils;

import android.os.AsyncTask;
import android.util.Log;

import com.erybo.Interfaces.ProcessResponse;

import org.codehaus.jackson.map.ObjectMapper;

/**
 * Created by Erybo on 2/4/16.
 */
public class BackgroundParser<T> extends AsyncTask <Object, Void, T>{

    private final String TAG = this.getClass().getSimpleName();
    private Class<T> type;
    private ProcessResponse<T> func;

    public BackgroundParser(Class<T> classType, ProcessResponse<T> f) {
        this.type = classType;
        this.func = f;
    }

    protected void onPostExecute(T response)
    {
        func.process(response);
    }

    @Override
    protected T doInBackground(Object... params) {

        int count = params.length;

        for (int i = 0; i < count; i++) {
            ObjectMapper mapper = new ObjectMapper();
            return (T)mapper.convertValue(params[i], this.type);
        }
        return null;
    }
}
