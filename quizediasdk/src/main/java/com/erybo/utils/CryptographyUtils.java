package com.erybo.utils;

import android.util.Base64;
import android.util.Log;

import com.erybo.quizediasdk.NetworkConnection;

import java.io.FileNotFoundException;
import java.io.InputStream;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.PublicKey;
import java.security.SecureRandom;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.util.Random;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

/**
 * Created by Erybo on 2/3/16.
 */
public class CryptographyUtils {

    static final int IV_SIZE = 16;

    public static String getCipherTextRSA(InputStream fin) throws Exception {
        NetworkConnection.getInstance().setMyTempKey(rsaTempKeyGenerator());
        Cipher cipher = Cipher.getInstance("RSA/ECB/OAEPWithSHA1AndMGF1Padding", "BC");
        cipher.init(Cipher.ENCRYPT_MODE, getKeyFromCert(fin), new SecureRandom());
        byte[] cipherText = cipher.doFinal(NetworkConnection.getInstance().getMyTempKey());
        NetworkConnection.getInstance().setBase64StringEncodedCipherText(Base64.encodeToString(cipherText, Base64.DEFAULT));
        return NetworkConnection.getInstance().getBase64StringEncodedCipherText();
    }

    public static byte[] encryptRSA(byte[] input,InputStream fin) throws Exception {

        Cipher cipher = Cipher.getInstance("RSA/ECB/OAEPWithSHA1AndMGF1Padding", "BC");
        cipher.init(Cipher.ENCRYPT_MODE, getKeyFromCert(fin), new SecureRandom());
        byte[] cipherText = cipher.doFinal(input);
        return cipherText;
    }

    public static PublicKey getKeyFromCert(InputStream fin) throws FileNotFoundException, CertificateException {
        CertificateFactory f = CertificateFactory.getInstance("X.509");
        X509Certificate certificate = (X509Certificate) f.generateCertificate(fin);
        return certificate.getPublicKey();
    }

    public static String encryptAndBase64EncodeAES (String jsonMessageString) throws NoSuchPaddingException, NoSuchAlgorithmException, NoSuchProviderException, InvalidAlgorithmParameterException, InvalidKeyException, BadPaddingException, IllegalBlockSizeException {

        NetworkConnection.getInstance().setMyTempKey(rsaTempKeyGenerator());

        SecretKeySpec skeySpec = new SecretKeySpec(NetworkConnection.getInstance().getDecryptedKeyRaw(), "AES");
        IvParameterSpec ivspec = new IvParameterSpec(NetworkConnection.getInstance().getMyTempKey());

        Cipher cipher = Cipher.getInstance("AES/CBC/PKCS7Padding", "BC");
        cipher.init(Cipher.ENCRYPT_MODE, skeySpec, ivspec);
        byte[] encrypted = cipher.doFinal(jsonMessageString.getBytes());
        return Base64.encodeToString(encrypted, Base64.DEFAULT);
    }
    public static String decryptAES (byte[] c) throws NoSuchPaddingException, NoSuchAlgorithmException, NoSuchProviderException, BadPaddingException, IllegalBlockSizeException, InvalidAlgorithmParameterException, InvalidKeyException {

        SecretKeySpec skeySpec = new SecretKeySpec(NetworkConnection.getInstance().getMyTempKey(), "AES");
        IvParameterSpec ivspec = new IvParameterSpec(Base64.decode(NetworkConnection.getInstance().getIv(), Base64.DEFAULT));

        Cipher cipher = Cipher.getInstance("AES/CBC/PKCS7Padding", "BC");
        cipher.init(Cipher.DECRYPT_MODE, skeySpec, ivspec);
        byte[] decrypted = cipher.doFinal(c);
        NetworkConnection.getInstance().setDecryptedKeyRaw(decrypted);
        return new String(decrypted);
    }

    public static String decryptAESServerKey (byte[] c) throws NoSuchPaddingException, NoSuchAlgorithmException, NoSuchProviderException, BadPaddingException, IllegalBlockSizeException, InvalidAlgorithmParameterException, InvalidKeyException {

        SecretKeySpec skeySpec = new SecretKeySpec(NetworkConnection.getInstance().getDecryptedKeyRaw(), "AES");
        IvParameterSpec ivspec = new IvParameterSpec(Base64.decode(NetworkConnection.getInstance().getIv(), Base64.DEFAULT));

        Cipher cipher = Cipher.getInstance("AES/CBC/PKCS7Padding", "BC");
        cipher.init(Cipher.DECRYPT_MODE, skeySpec, ivspec);
        byte[] decrypted = cipher.doFinal(c);
        NetworkConnection.getInstance().setDecryptedKeyRaw(decrypted);
        return new String(decrypted);
    }

    public static String decryptAES(String base64EncodedString) throws NoSuchPaddingException, InvalidKeyException, NoSuchAlgorithmException, IllegalBlockSizeException, BadPaddingException, NoSuchProviderException, InvalidAlgorithmParameterException {
        return decryptAES(Base64.decode(base64EncodedString, Base64.DEFAULT));
    }

    public static String decryptAESServerKey(String base64EncodedString) throws NoSuchPaddingException, InvalidKeyException, NoSuchAlgorithmException, IllegalBlockSizeException, BadPaddingException, NoSuchProviderException, InvalidAlgorithmParameterException {
        return decryptAESServerKey(Base64.decode(base64EncodedString, Base64.DEFAULT));
    }

    public static byte[] rsaTempKeyGenerator(){
        byte[] tempKey = new byte[IV_SIZE];
        new Random().nextBytes(tempKey);
        return tempKey;
    }

}
