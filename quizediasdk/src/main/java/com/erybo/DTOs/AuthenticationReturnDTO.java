package com.erybo.DTOs;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

/**
 * Created by Erybo on 2/5/16.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class AuthenticationReturnDTO {

    private String incompatible;
    private String serverVersion;
    private String u;
    private String c;
    private String iv;
    private List<PublishSubscribeChannelDTO> pubsub;
    private String flags;
    private List<QSONTemplateDTO> qsontempl;
    private List<PathDTO> paths;
    private String p;
    private String e;

    public String getIncompatible() {
        return incompatible;
    }

    public void setIncompatible(String incompatible) {
        this.incompatible = incompatible;
    }

    public String getServerVersion() {
        return serverVersion;
    }

    public void setServerVersion(String serverVersion) {
        this.serverVersion = serverVersion;
    }

    public String getUserId () {
        return u;
    }
    public void setU(String u) {
        this.u = u;
    }

    public String getC() {
        return c;
    }
    public void setC(String c) {
        this.c = c;
    }

    public String getIv() {
        return iv;
    }
    public void setIv(String iv) {
        this.iv = iv;
    }

    public void setPubsub(List<PublishSubscribeChannelDTO> pubsub) {
        this.pubsub = pubsub;
    }

    public List<PublishSubscribeChannelDTO> getPubsub() {
        return pubsub;
    }

    public String getFlags() {
        return flags;
    }

    public void setFlags(String flags) {
        this.flags = flags;
    }

    public List<QSONTemplateDTO> getQsontempl() {
        return qsontempl;
    }

    public void setQsontempl(List<QSONTemplateDTO> qsontempl) {
        this.qsontempl = qsontempl;
    }

    public List<PathDTO> getPaths() {
        return paths;
    }

    public void setPaths(List<PathDTO> paths) {
        this.paths = paths;
    }

    public String getP() {
        return p;
    }

    public void setP(String p) {
        this.p = p;
    }

    public String getE() {
        return e;
    }

    public void setE(String e) {
        this.e = e;
    }

    public void populateEmailPassUserIdFromDecodedJson (String jsonString) {
        try {

            JSONObject userDetailsObject = new JSONObject(jsonString);
            setP(userDetailsObject.getString("p"));
            setE(userDetailsObject.getString("e"));
            setU(userDetailsObject.getString("u"));
        } catch (JSONException e1) {
            e1.printStackTrace();
        }
    }
}
