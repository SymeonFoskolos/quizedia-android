package com.erybo.DTOs;

/**
 * Created by Erybo on 2/9/16.
 */
public class RankingDTO {

    private String t;
    private String p;

    public String getTopicId() {
        return t;
    }

    public void setT(String t) {
        this.t = t;
    }

    public String getPercentage() {
        return p;
    }

    public void setP(String p) {
        this.p = p;
    }
}
