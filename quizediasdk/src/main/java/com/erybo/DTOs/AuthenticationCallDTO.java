package com.erybo.DTOs;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

/**
 * Created by Erybo on 2/8/16.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class AuthenticationCallDTO {

    private String n;
    private String f;
    private String e;
    private String p;
    private DeviceMetadataDTO d;

    public AuthenticationCallDTO () {

    }

    public AuthenticationCallDTO(String n, String f, String e, String p, DeviceMetadataDTO d) {
        setN(n);
        setF(f);
        setE(e);
        setP(p);
        setD(d);
    }
    public String getN() {
        return n;
    }
    public void setN(String n) {
        this.n = n;
    }

    public String getF() {
        return f;
    }
    public void setF(String f) {
        this.f = f;
    }

    public String getE() {
        return e;
    }
    public void setE(String e) {
        this.e = e;
    }

    public String getP() {
        return p;
    }
    public void setP(String p) {
        this.p = p;
    }

    public DeviceMetadataDTO getD() {
        return d;
    }
    public void setD(DeviceMetadataDTO d) {
        this.d = d;
    }
}
