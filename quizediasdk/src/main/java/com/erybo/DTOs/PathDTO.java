package com.erybo.DTOs;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

/**
 * Created by Erybo on 2/8/16.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class PathDTO {
    private String t;
    private String p;

    public String getT() {
        return t;
    }

    public void setT(String t) {
        this.t = t;
    }

    public String getP() {
        return p;
    }

    public void setP(String p) {
        this.p = p;
    }
}
