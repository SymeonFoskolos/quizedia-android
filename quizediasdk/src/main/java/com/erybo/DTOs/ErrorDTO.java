package com.erybo.DTOs;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

/**
 * Created by Erybo on 2/9/16.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class ErrorDTO {

    private String c;
    private String d;

    public String getErrorCode() {
        return c;
    }

    public void setC(String c) {
        this.c = c;
    }

    public String getDescription() {
        return d;
    }

    public void setD(String d) {
        this.d = d;
    }
}
