package com.erybo.DTOs;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

import java.util.List;

/**
 * Created by Erybo on 2/9/16.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class UserProfileReturnDTO {

    private UserProfileDTO p;
    private List<RankingDTO> r;
    private ErrorDTO err;

    public UserProfileDTO getP() {
        return p;
    }

    public void setP(UserProfileDTO p) {
        this.p = p;
    }

    public List<RankingDTO> getR() {
        return r;
    }

    public void setR(List<RankingDTO> r) {
        this.r = r;
    }

    public ErrorDTO getErr() {
        return err;
    }

    public void setErr(ErrorDTO err) {
        this.err = err;
    }
}
