package com.erybo.DTOs;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

/**
 * Created by Erybo on 2/8/16.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class PublishSubscribeChannelDTO {
    private String sub;
    private String pub;
    private String uri;

    public String getSub() {
        return sub;
    }

    public void setSub(String sub) {
        this.sub = sub;
    }

    public String getPub() {
        return pub;
    }

    public void setPub(String pub) {
        this.pub = pub;
    }

    public String getUri() {
        return uri;
    }

    public void setUri(String uri) {
        this.uri = uri;
    }
}
