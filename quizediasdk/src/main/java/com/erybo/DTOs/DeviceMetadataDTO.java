package com.erybo.DTOs;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

/**
 * Created by Erybo on 2/5/16.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class DeviceMetadataDTO {

    private String osType;
    private String osVersion;
    private String deviceModel;
    private String deviceType;
    private String appVersion;
    private String locale;
    private String uid;
    private String msisdn;

    public DeviceMetadataDTO() {

    }

    public DeviceMetadataDTO (String osType, String osVersion, String deviceModel, String deviceType, String appVersion, String locale, String uid, String msisdn ){
        setOsType(osType);
        setOsVersion(osVersion);
        setDeviceModel(deviceModel);
        setDeviceType(deviceType);
        setAppVersion(appVersion);
        setLocale(locale);
        setUid(uid);
        setMsisdn(msisdn);
    }

    public String getOsType() {
        return osType;
    }

    public void setOsType(String osType) {
        this.osType = osType;
    }

    public String getOsVersion() {
        return osVersion;
    }

    public void setOsVersion(String osVersion) {
        this.osVersion = osVersion;
    }

    public String getDeviceModel() {
        return deviceModel;
    }

    public void setDeviceModel(String deviceModel) {
        this.deviceModel = deviceModel;
    }

    public String getDeviceType() {
        return deviceType;
    }

    public void setDeviceType(String deviceType) {
        this.deviceType = deviceType;
    }

    public String getAppVersion() {
        return appVersion;
    }

    public void setAppVersion(String appVersion) {
        this.appVersion = appVersion;
    }

    public String getLocale() {
        return locale;
    }

    public void setLocale(String locale) {
        this.locale = locale;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getMsisdn() {
        return msisdn;
    }

    public void setMsisdn(String msisdn) {
        this.msisdn = msisdn;
    }
}
