package com.erybo.DTOs;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

/**
 * Created by Erybo on 2/4/16.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class AuthReqDTO{

    private String c;
    private String iv;

    public String getEncryptedKey() {
        return this.c;
    }

    public String getSeed() {
        return this.iv;
    }

    public void setC(String c) {
        this.c = c;
    }

    public void setIv(String iv) {
        this.iv = iv;
    }
}
