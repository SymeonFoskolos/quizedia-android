package com.erybo.DTOs;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

/**
 * Created by Erybo on 2/8/16.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class QSONTemplateDTO {
    private int id;
    private String path;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }
}
